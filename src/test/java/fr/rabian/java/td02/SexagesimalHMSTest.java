package fr.rabian.java.td02;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SexagesimalHMSTest {
    SexagesimalHMS sfhms;
    SexagesimalHMS sff;
    static double delta;

    @Before
    public void setUp() throws Exception {
        sfhms = new SexagesimalHMS(1, 36, 36);
        sff = new SexagesimalHMS(1.624);
        delta = Math.pow(10,-3);
    }

    @After
    public void tearDown() throws Exception {
        sfhms = null;
        sff = null;
        delta = 0;
    }

    @Test
    public void testGetDec() {
        assertEquals("Obtention de la valeur décimale (flottant)", 1.624, sff.getDec(), delta);
        assertEquals("Obtention de la valeur décimale (HMS)", 1.61, sfhms.getDec(), delta);
    }

    @Test
    public void testGetH() {
        assertEquals("Obtention de l'heure (flottant)", 1, sff.getH());
        assertEquals("Obtention de l'heure (HMS)", 1, sfhms.getH());
    }

    @Test
    public void testGetM() {
        assertEquals("Obtention des minutes (flottant)", 37, sff.getM());
        assertEquals("Obtention des minutes (HMS)", 36, sfhms.getM());
    }

    @Test
    public void testGetS() {
        assertEquals("Obtention des minutes (flottant)", 26, sff.getS());
        assertEquals("Obtention des minutes (HMS)", 36, sfhms.getS());
    }

    @Test
    public void testToString() {
        assertEquals("Transformation en chaîne (flottant)", "01:37:26", sff.toString());
        assertEquals("Transformation en chaîne (HMS)", "01:36:36", sfhms.toString());
    }
}