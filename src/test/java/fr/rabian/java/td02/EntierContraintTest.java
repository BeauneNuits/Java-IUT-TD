package fr.rabian.java.td02;

import org.junit.*;
import static org.junit.Assert.*;

public class EntierContraintTest {
    EntierContraint i1;

    @Before
    public void setUp() throws Exception {
        i1 = new EntierContraint(5, 10, 7);
    }

    @After
    public void tearDown() throws Exception {
        i1 = null;
    }

    @Test
    public void testGetMin() throws Exception {
        assertEquals("Obtention du minimum", 5, i1.getMin());
    }

    @Test
    public void testGetMax() throws Exception {
        assertEquals("Obtention du maximum", 10, i1.getMax());
    }

    @Test
    public void testGetVal() throws Exception {
        assertEquals("Obtention de la valeur", 7, i1.getVal());
    }

    @Test
    public void testSetVal() throws Exception {
        i1.setVal(9);
        assertEquals("Modif. valeur", 9, i1.getVal());
    }

    @Test
    public void testToString() throws Exception {
        assertEquals("Transformation en chaîne", "7 in [5,10]", i1.toString());
    }

    @Test
    public void testIncrement() throws Exception {
        i1.increment(3);
        assertEquals("Incrémentation", 10, i1.getVal());
    }

    @Test (expected = ArithmeticException.class)
    public void testMauvaisIntervalle() throws Exception {
        i1 = new EntierContraint(6, 3);
    }

    @Test (expected = ArithmeticException.class)
    public void testHorsIntervalle() throws Exception {
        i1 = new EntierContraint(6, 14, 3);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testSetHorsIntervalle() throws Exception {
        i1.setVal(2);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testIncHorsIntervalle() throws Exception {
        i1.increment(10);
    }
}