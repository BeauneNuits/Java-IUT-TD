package fr.rabian.java.td06;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Point2DTest {
    private final Point2D p1 = new Point2D(2, 3);
    private Point2D p2;
    private Point2D p3;

    @Before
    public void setUp() throws Exception {
        p2 = p1.clone();
        p3 = new Point2D(2, 3);
    }

    @After
    public void tearDown() throws Exception {
        p2 = null;
        p3 = null;
    }

    @Test
    public void testEgaliteClone() throws Exception {
        assertTrue("Egalité de clone.", p2.equals(p1));
    }

    @Test
    public void testDeplacement() throws Exception {
        p2.deplace(1, 1);
        assertTrue("Modification de p2", p2.getX() == 3 & p2.getY() == 4);
        assertTrue("Non-modification de p1", p1.getX()==2 & p1.getY()==3);
    }

    @Test
    public void testSymetrie() throws Exception {
        assertTrue("Symetrie sens direct", p2.equals(p1));
        assertTrue("Symétrie sens inverse", p1.equals(p2));
    }

    @Test
    public void testReflexivite() throws Exception {
        assertTrue("testReflexivite", p2.equals(p2));
    }

    @Test
    public void testNull() throws Exception {
        assertFalse(p2.equals(null));
    }

    @Test
    public void testTransitivite() throws Exception {
        assertTrue("1 vers 2", p1.equals(p2));
        assertTrue("2 vers 3", p2.equals(p3));
        assertTrue("1 vers 3", p1.equals(p3));
    }

}