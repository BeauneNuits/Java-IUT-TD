package fr.rabian.java.td03;

import org.junit.*;
import static org.junit.Assert.*;

public class CompteurTest {
    private Compteur c;

    @Before
    public void setUp() throws Exception {
        c = new Compteur();
    }

    @After
    public void tearDown() throws Exception {
        c = null;
    }

    @Test
    public void testGetIdMax() throws Exception {
        assertEquals("Egalité idMax et id lors de la création", Compteur.getIdMax(), c.getId());
    }

    @Test
    public void testGetId() throws Exception {
        assertTrue("Obtention id", c.getId()==2 || c.getId()==1);
    }
}