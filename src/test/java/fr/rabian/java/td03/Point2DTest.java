package fr.rabian.java.td03;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Point2DTest {
    private Point2D p;
    private final Point2D O = new Point2D();
    private final double delta = Math.pow(10, -10);

    @Before
    public void setUp() throws Exception {
        p = new Point2D(2, 0);
    }

    @After
    public void tearDown() throws Exception {
        p = null;
    }

    @Test
    public void testDistance() throws Exception {
        assertEquals("Distance", 2f, O.distance(p), delta);
    }

    @Test
    public void testRotation() throws Exception {
        p.rotation(Math.PI/2);
        assertEquals("Distance après rotation", 2f, O.distance(p), delta);
        assertEquals("Abcisse après rotation", 0, p.getX(), delta);
        assertEquals("Ordonnée après rotation", 2f, p.getY(), delta);
    }
}