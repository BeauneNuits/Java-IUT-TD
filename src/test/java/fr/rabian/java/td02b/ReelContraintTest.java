package fr.rabian.java.td02b;

import org.junit.*;
import static org.junit.Assert.*;

public class ReelContraintTest {
    ReelContraint i1;
    static double delta;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        delta = Math.pow(10, -10);
    }

    @Before
    public void setUp() throws Exception {
        i1 = new ReelContraint(Math.PI, 2*Math.PI, 4.12);
    }

    @After
    public void tearDown() throws Exception {
        i1 = null;
    }

    @Test
    public void testGetMin() throws Exception {
        assertEquals("Obtention du minimum", Math.PI, i1.getMin(), delta);
    }

    @Test
    public void testGetMax() throws Exception {
        assertEquals("Obtention du maximum", 2*Math.PI, i1.getMax(), delta);
    }

    @Test
    public void testGetVal() throws Exception {
        assertEquals("Obtention de la valeur", 4.12, i1.getVal(), delta);
    }

    @Test
    public void testSetVal() throws Exception {
        double newVal = 3.59;
        i1.setVal(newVal);
        assertEquals("Modif. valeur", newVal, i1.getVal(), delta);
    }

    @Test
    public void testToString() throws Exception {
        assertEquals("Transformation en chaîne", "4.12", i1.toString());
    }

    @Test
    public void testIncrement() throws Exception {
        double incVal = 1.56;
        i1.increment(incVal);
        assertEquals("Incrémentation", incVal+4.12, i1.getVal(), delta);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMauvaisIntervalle() throws Exception {
        i1 = new ReelContraint(5.13, 2.45);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testHorsIntervalle() throws Exception {
        i1 = new ReelContraint(6.45, 14.02, 3.634);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testSetHorsIntervalle() throws Exception {
        i1.setVal(2.45);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testIncHorsIntervalle() throws Exception {
        i1.increment(5.345);
    }
}