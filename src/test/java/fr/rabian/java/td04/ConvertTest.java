package fr.rabian.java.td04;

import org.junit.Test;
import static org.junit.Assert.*;

public class ConvertTest {
    private final double delta = Math.pow(10, -6);

    @Test
    public void testBooleanToString() throws Exception {
        assertTrue("Boolean to String", "true".equals(Convert.booleanToString(true)));
    }

    @Test
    public void testByteToString() throws Exception {
        assertTrue("Byte to String", "3".equals(Convert.byteToString((byte) 3)));
    }

    @Test
    public void testShortToString() throws Exception {
        assertTrue("Short to String", "120".equals(Convert.shortToString((short) 120)));
    }

    @Test
    public void testIntToString() throws Exception {
        assertTrue("Int to String", "11456".equals(Convert.intToString(11456)));
    }

    @Test
    public void testLongToString() throws Exception {
        assertTrue("Long to String", "123456789".equals(Convert.longToString((long) 123456789)));
    }

    @Test
    public void testFloatToString() throws Exception {
        assertTrue("Float to String", "12.56".equals(Convert.floatToString((float) 12.56)));
    }

    @Test
    public void testDoubleToString() throws Exception {
        assertTrue("Double to String", "11456.123456789".equals(Convert.doubleToString(11456.123456789)));
    }

    @Test
    public void testCharToString() throws Exception {
        assertTrue("Char to String", "g".equals(Convert.charToString('g')));
    }

    @Test
    public void testStringToBoolean() throws Exception {
        assertTrue("String to Boolean", Convert.stringToBoolean("true"));
    }

    @Test
    public void testStringToByte() throws Exception {
        assertEquals("String to Byte", 3, Convert.stringToByte("3"));
    }

    @Test
    public void testStringToShort() throws Exception {
        assertEquals("String to Short", 12, Convert.stringToShort("12"));
    }

    @Test
    public void testStringToInt() throws Exception {
        assertEquals("String to Int", 1342, Convert.stringToInt("1342"));
    }

    @Test
    public void testStringToLong() throws Exception {
        assertEquals("String to Long", 124561209, Convert.stringToLong("124561209"));
    }

    @Test
    public void testStringToFloat() throws Exception {
        assertEquals("String to Float", 13.42, Convert.stringToFloat("13.42"), delta);
    }

    @Test
    public void testStringToDouble() throws Exception {
        assertEquals("String to Double", 12456.1209, Convert.stringToDouble("12456.1209"), delta);
    }

    @Test
    public void testStringToChar() throws Exception {
        assertTrue("String to char", 'y'==Convert.stringToChar("y"));
    }

    @Test
    public void testStringCompareToBoolean() throws Exception {
        assertEquals("String compare to Boolean", 0, Convert.stringCompareToBoolean("true", true));
    }

    @Test
    public void testStringCompareToByte() throws Exception {
        assertTrue("String compare to Byte", Convert.stringCompareToByte("3", (byte) 8)<0);
    }

    @Test
    public void testStringCompareToShort() throws Exception {
        assertTrue("String compare to Short", Convert.stringCompareToShort("12", (short) 8)>0);
    }

    @Test
    public void testStringCompareToInt() throws Exception {
        assertTrue("String compare to Int", Convert.stringCompareToInt("145", 1234)<0);
    }

    @Test
    public void testStringCompareToLong() throws Exception {
        assertEquals("String compare to Long", 0, Convert.stringCompareToLong("3456789", (long) 3456789));
    }

    @Test
    public void testStringCompareToFloat() throws Exception {
        assertTrue("String compare to Float", Convert.stringCompareToFloat("12.456", (float) 12.4569)<0);
    }

    @Test
    public void testStringCompareToDouble() throws Exception {
        assertTrue("String compare to Double", Convert.stringCompareToDouble("34.5634", 34.5633)>0);
        assertEquals("String compare to Double", 0, Convert.stringCompareToDouble("34.5634", 34.5634));
    }

    @Test
    public void testStringCompareToChar() throws Exception {
        assertTrue("String compare to Char", Convert.stringCompareToChar("a", 'b')<0);
    }
}