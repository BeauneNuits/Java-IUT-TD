package fr.rabian.java.td04;

import org.junit.Test;

import static org.junit.Assert.*;

public class UtilStringTest {

    @Test
    public void testNbOccurences() throws Exception {
        assertEquals(2, UtilString.nbOccurences("abaabb", "ab"));
    }
}