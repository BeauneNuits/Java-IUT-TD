package fr.rabian.java.td05;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Point2DAffichableTest {
    private Point2DAffichable a;

    @Before
    public void setUp() throws Exception {
        a = new Point2DAffichable();
        a.initialise(2,3);
    }

    @After
    public void tearDown() throws Exception {
        a = null;
    }

    @Test
    public void testGetX() throws Exception {
        assertEquals(2, a.getX());
    }

    @Test
    public void testGetY() throws Exception {
        assertEquals(3, a.getY());
    }
}