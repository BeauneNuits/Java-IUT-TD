package fr.rabian.java.td05;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Point3DTest {
    private Point3D pt;

    @Before
    public void setUp() throws Exception {
        pt = new Point3D();
        pt.initialise(1, 2, 3);
    }

    @After
    public void tearDown() throws Exception {
        pt = null;
    }

    @Test
    public void testGetZ() throws Exception {
        assertEquals(3, pt.getZ());
    }

    @Test
    public void testGetX() throws Exception {
        assertEquals(1, pt.getX());
    }

    @Test
    public void testGetY() throws Exception {
        assertEquals(2, pt.getY());
    }
}