package aspe.java.td02b;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConstrainedRealTest {
    ConstrainedReal i1;
    static double delta;
    static double val;
    static double min;
    static double max;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        delta = Math.pow(10, -10);
        val = 4.12;
        min = Math.PI;
        max = 2* Math.PI;
    }

    @Before
    public void setUp() throws Exception {
        i1 = new ConstrainedReal(min, max, val);
    }

    @After
    public void tearDown() throws Exception {
        i1 = null;
    }

    @Test
    public void testGetMin() throws Exception {
        assertEquals("Obtention du minimum", min, i1.getMin(), delta);
    }

    @Test
    public void testGetMax() throws Exception {
        assertEquals("Obtention du maximum", max, i1.getMax(), delta);
    }

    @Test
    public void testGetVal() throws Exception {
        assertEquals("Obtention de la valeur", val, i1.getVal(), delta);
    }

    @Test
    public void testSetVal() throws Exception {
        double newVal = 3.59;
        i1.setVal(newVal);
        assertEquals("Modif. valeur", newVal, i1.getVal(), delta);
    }

    @Test (expected = ArithmeticException.class)
    public void testMauvaisIntervalle() throws Exception {
        i1 = new ConstrainedReal(5.13, 2.45);
    }

    @Test (expected = ArithmeticException.class)
    public void testHorsIntervalle() throws Exception {
        i1 = new ConstrainedReal(6.45, 14.02, 3.634);
    }

    @Test (expected = ArithmeticException.class)
    public void testSetHorsIntervalle() throws Exception {
        i1.setVal(2.45);
    }
}