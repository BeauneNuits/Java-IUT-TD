package aspe.java.td03.tests;

/*
L'instruction suivante ne fonctionne pas. En effet :
un.package.* importe seulement les classes contenues directement dans un.package
et n'importe pas récursivement les classes contenues dans des sous-packages.
*/

// import info.augendre.java.td03.geometry.*
import aspe.java.td03.geometry.oneDim.Segment2D;
import aspe.java.td03.geometry.twoDim.Triangle2D;
import aspe.java.td03.geometry.zeroDim.Point2D;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Triangle2DTest {
    private Triangle2D t;
    private Point2D p1;
    private Point2D p2;
    private Point2D p3;

    private static final double DELTA = Math.pow(10, -5);

    @Before
    public void setUp() throws Exception {
        p1 = new Point2D(3, 1);
        p2 = new Point2D(5, 1);
        p3 = new Point2D(3, 4);
        t = new Triangle2D(p1, p2, p3);
    }

    @After
    public void tearDown() throws Exception {
        t = null;
        p1 = null;
        p2 = null;
        p3 = null;
    }

    @Test
    public void testSurface() throws Exception {
        assertEquals("Test surface", 3.0, t.surface(), DELTA);
    }

    @Test
    public void testPerimeter() throws Exception {
        Segment2D s1 = new Segment2D(p1, p2);
        Segment2D s2 = new Segment2D(p2, p3);
        Segment2D s3 = new Segment2D(p3, p1);
        double peri = s1.longueur() + s2.longueur() + s3.longueur();
        assertEquals("Test périmètre", peri, t.perimetre(), DELTA);
    }
}