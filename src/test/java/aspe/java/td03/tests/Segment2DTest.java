package aspe.java.td03.tests;

/*
L'instruction suivante ne fonctionne pas. En effet :
un.package.* importe seulement les classes contenues directement dans un.package
et n'importe pas récursivement les classes contenues dans des sous-packages.
*/

// import info.augendre.java.td03.geometry.*
import aspe.java.td03.geometry.oneDim.Segment2D;
import aspe.java.td03.geometry.zeroDim.Point2D;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Segment2DTest {
    private Segment2D s;
    private Point2D p1;
    private Point2D p2;

    private final static double DELTA = Math.pow(10, -6);

    @Before
    public void setUp() throws Exception {
        p1 = new Point2D(1, 2);
        p2 = new Point2D(0, -4);
        s = new Segment2D(p1, p2);
    }

    @After
    public void tearDown() throws Exception {
        s = null;
        p1 = null;
        p2 = null;
    }

    @Test
    public void testLongueur() throws Exception {
        assertEquals("Test longueur du segment", Math.sqrt(37), s.longueur(), DELTA);
    }

    @Test
    public void testToString() throws Exception {
        assertEquals("Test affichage", "De [1.0, 2.0] à [0.0, -4.0].", s.toString());
    }

    @Test
    public void testDeplaceP1X() throws Exception {
        s.deplaceP1(0.1, 0);
        assertEquals("Modification P1 x", 1.1, p1.getX(), DELTA);
    }

    @Test
    public void testDeplaceP1Y() throws Exception {
        s.deplaceP1(0, 0.1);
        assertEquals("Modification P1 y", 2.1, p1.getY(), DELTA);
    }

    @Test
    public void testDeplaceP2X() throws Exception {
        s.deplaceP2(0.1, 0);
        assertEquals("Modification P2 x", 0.1, p2.getX(), DELTA);
    }

    @Test
    public void testDeplaceP2Y() throws Exception {
        s.deplaceP2(0, 0.1);
        assertEquals("Modification P2 y", -3.9, p2.getY(), DELTA);
    }

    @Test
    public void testGetP1() throws Exception {
        assertEquals("Récupération de P1", p1, s.getP1());
    }

    @Test
    public void testGetP2() throws Exception {
        assertEquals("Récupération de P2", p2, s.getP2());
    }
}