package aspe.java.td03.tests;

/*
L'instruction suivante ne fonctionne pas. En effet :
un.package.* importe seulement les classes contenues directement dans un.package
et n'importe pas récursivement les classes contenues dans des sous-packages.
*/

// import info.augendre.java.td03.geometry.*
import aspe.java.td03.geometry.oneDim.Segment2D;
import aspe.java.td03.geometry.twoDim.Rectangle2D;
import aspe.java.td03.geometry.zeroDim.Point2D;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Rectangle2DTest {
    /**
     * Un rectangle construit à partir de deux points.
     */
    private Rectangle2D r2;

    /**
     * Un rectangle construit à partir de trois points.
     */
    private Rectangle2D r3;

    /**
     * Point composant R3.
     */
    private Point2D p1;

    /**
     * Point composant R2 et R3.
     */
    private Point2D p2;

    /**
     * Point composant R2 et R3.
     */
    private Point2D p3;

    /**
     * Un des côtés de R3.
     */
    private Segment2D s1;

    /**
     * L'autre côté de R3.
     */
    private Segment2D s2;

    private final static double DELTA = Math.pow(10, -5);

    @Before
    public void setUp() throws Exception {
        p1 = new Point2D(4, 1);
        p2 = new Point2D(2, 3);
        p3 = new Point2D(8, 5);
//        p4 = new Point2D(6, 7); // Le dernier point de p3, pour information

        // Ce sont deux des segments de R3
        s1 = new Segment2D(p1, p2);
        s2 = new Segment2D(p1, p3);
        r3 = new Rectangle2D(p1, p2, p3);


        r2 = new Rectangle2D(p2, p3);
    }

    @After
    public void tearDown() throws Exception {
        r2 = null;
        r3 = null;

        s1 = null;
        s2 = null;

        p1 = null;
        p2 = null;
        p3 = null;
    }

    @Test
    public void testSurfaceR2() throws Exception {
        assertEquals("Test surface r2", 12, r2.surface(), DELTA);
    }

    @Test
    public void testSurfaceR3() throws Exception {
        double surfaceR3 = s1.longueur() * s2.longueur();
        assertEquals("Test surface r3", surfaceR3, r3.surface(), DELTA);
    }

    @Test
    public void testPerimetreR2() throws Exception {
        assertEquals("Test surface r2", 16, r2.perimetre(), DELTA);
    }

    @Test
    public void testPerimetreR3() throws Exception {
        double perimetreR3 = 2 * (s1.longueur() + s2.longueur());
        assertEquals("Test surface r3", perimetreR3, r3.perimetre(), DELTA);
    }
}