package aspe.java.td03;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CompteurTest {
    private Compteur c;

    @Before
    public void setUp() throws Exception {
        c = new Compteur();
    }

    @After
    public void tearDown() throws Exception {
        c = null;
    }

    @Test
    public void testGetIdMax() throws Exception {
        assertEquals("Egalité idMax et id lors de la création", Compteur.getIdMax(), c.getId());
    }

    @Test
    public void testGetId() throws Exception {
        assertTrue("Obtention id", c.getId()==2 || c.getId()==1);
    }
}