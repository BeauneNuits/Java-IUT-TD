package aspe.java.td04;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RepertoireEtudiantsTest {

    private static Etudiant e2;
    private static Etudiant e3;
    private static RepertoireEtudiants r;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        Etudiant e1 = new Etudiant("Rabian", "Adrien");
        e2 = new Etudiant("Augendre", "Gabriel");
        e3 = new Etudiant("Fenouil", "Cyril");
        r = new RepertoireEtudiants(10);
        r.ajouteEtudiant(e1);
        r.ajouteEtudiant(e2);
    }

    @Test
    public void testAjouteEtudiant() throws Exception {
        assertTrue(r.ajouteEtudiant(e3));
        assertTrue(r.supprimeEtudiant(e3));
    }

    @Test
    public void testGetNom() throws Exception {
        assertEquals("Rabian", r.getNom(1));
    }

    @Test
    public void testGetNumero() throws Exception {
        assertTrue(r.getNumero("rabian") > 0);
    }

    @Test
    public void testSupprimeEtudiant() throws Exception {
        assertTrue(r.supprimeEtudiant(e2));
    }
}