package aspe.java.td04;

import fr.rabian.java.td04.UtilString;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilStringTest {

    @Test
    public void testNbOccurences() throws Exception {
        assertEquals(2, UtilString.nbOccurences("abaabb", "ab"));
    }
}