package aspe.java.td04;

import fr.rabian.java.td04.Convert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ConvertTest {
    private final double delta = Math.pow(10, -6);

    @Test
    public void testBooleanToString() throws Exception {
        assertTrue("Boolean to String", "true".equals(fr.rabian.java.td04.Convert.booleanToString(true)));
    }

    @Test
    public void testByteToString() throws Exception {
        assertTrue("Byte to String", "3".equals(fr.rabian.java.td04.Convert.byteToString((byte) 3)));
    }

    @Test
    public void testShortToString() throws Exception {
        assertTrue("Short to String", "120".equals(fr.rabian.java.td04.Convert.shortToString((short) 120)));
    }

    @Test
    public void testIntToString() throws Exception {
        assertTrue("Int to String", "11456".equals(fr.rabian.java.td04.Convert.intToString(11456)));
    }

    @Test
    public void testLongToString() throws Exception {
        assertTrue("Long to String", "123456789".equals(fr.rabian.java.td04.Convert.longToString((long) 123456789)));
    }

    @Test
    public void testFloatToString() throws Exception {
        assertTrue("Float to String", "12.56".equals(fr.rabian.java.td04.Convert.floatToString((float) 12.56)));
    }

    @Test
    public void testDoubleToString() throws Exception {
        assertTrue("Double to String", "11456.123456789".equals(fr.rabian.java.td04.Convert.doubleToString(11456.123456789)));
    }

    @Test
    public void testCharToString() throws Exception {
        assertTrue("Char to String", "g".equals(fr.rabian.java.td04.Convert.charToString('g')));
    }

    @Test
    public void testStringToBoolean() throws Exception {
        assertTrue("String to Boolean", Convert.stringToBoolean("true"));
    }

    @Test
    public void testStringToByte() throws Exception {
        assertEquals("String to Byte", 3, fr.rabian.java.td04.Convert.stringToByte("3"));
    }

    @Test
    public void testStringToShort() throws Exception {
        assertEquals("String to Short", 12, fr.rabian.java.td04.Convert.stringToShort("12"));
    }

    @Test
    public void testStringToInt() throws Exception {
        assertEquals("String to Int", 1342, fr.rabian.java.td04.Convert.stringToInt("1342"));
    }

    @Test
    public void testStringToLong() throws Exception {
        assertEquals("String to Long", 124561209, fr.rabian.java.td04.Convert.stringToLong("124561209"));
    }

    @Test
    public void testStringToFloat() throws Exception {
        assertEquals("String to Float", 13.42, fr.rabian.java.td04.Convert.stringToFloat("13.42"), delta);
    }

    @Test
    public void testStringToDouble() throws Exception {
        assertEquals("String to Double", 12456.1209, fr.rabian.java.td04.Convert.stringToDouble("12456.1209"), delta);
    }

    @Test
    public void testStringToChar() throws Exception {
        assertTrue("String to char", 'y'== fr.rabian.java.td04.Convert.stringToChar("y"));
    }

    @Test
    public void testStringCompareToBoolean() throws Exception {
        assertEquals("String compare to Boolean", 0, fr.rabian.java.td04.Convert.stringCompareToBoolean("true", true));
    }

    @Test
    public void testStringCompareToByte() throws Exception {
        assertTrue("String compare to Byte", fr.rabian.java.td04.Convert.stringCompareToByte("3", (byte) 8)<0);
    }

    @Test
    public void testStringCompareToShort() throws Exception {
        assertTrue("String compare to Short", fr.rabian.java.td04.Convert.stringCompareToShort("12", (short) 8)>0);
    }

    @Test
    public void testStringCompareToInt() throws Exception {
        assertTrue("String compare to Int", fr.rabian.java.td04.Convert.stringCompareToInt("145", 1234)<0);
    }

    @Test
    public void testStringCompareToLong() throws Exception {
        assertEquals("String compare to Long", 0, fr.rabian.java.td04.Convert.stringCompareToLong("3456789", (long) 3456789));
    }

    @Test
    public void testStringCompareToFloat() throws Exception {
        assertTrue("String compare to Float", fr.rabian.java.td04.Convert.stringCompareToFloat("12.456", (float) 12.4569)<0);
    }

    @Test
    public void testStringCompareToDouble() throws Exception {
        assertTrue("String compare to Double", fr.rabian.java.td04.Convert.stringCompareToDouble("34.5634", 34.5633)>0);
        assertEquals("String compare to Double", 0, fr.rabian.java.td04.Convert.stringCompareToDouble("34.5634", 34.5634));
    }

    @Test
    public void testStringCompareToChar() throws Exception {
        assertTrue("String compare to Char", Convert.stringCompareToChar("a", 'b')<0);
    }
}