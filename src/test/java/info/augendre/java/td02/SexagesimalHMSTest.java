package info.augendre.java.td02;

import org.junit.*;

import static org.junit.Assert.*;

public class SexagesimalHMSTest {
    private SexagesimalHMS sfhms;
    private SexagesimalHMS sff;
    private static final double DELTA = Math.pow(10, -3);

    @Before
    public void setUp() throws Exception {
        sfhms = new SexagesimalHMS(1, 36, 36);
        sff = new SexagesimalHMS(1.624);
    }

    @After
    public void tearDown() throws Exception {
        sfhms = null;
        sff = null;
    }

    @Test
    public void testGetDec() {
        assertEquals("Obtention de la valeur décimale (flottant)", 1.624, sff.getDec(), DELTA);
        assertEquals("Obtention de la valeur décimale (HMS)", 1.61, sfhms.getDec(), DELTA);
    }

    @Test
    public void testGetH() {
        assertEquals("Obtention de l'heure (flottant)", 1, sff.getH());
        assertEquals("Obtention de l'heure (HMS)", 1, sfhms.getH());
    }

    @Test
    public void testGetM() {
        assertEquals("Obtention des minutes (flottant)", 37, sff.getM());
        assertEquals("Obtention des minutes (HMS)", 36, sfhms.getM());
    }

    @Test
    public void testGetS() {
        assertEquals("Obtention des minutes (flottant)", 26, sff.getS());
        assertEquals("Obtention des minutes (HMS)", 36, sfhms.getS());
    }

    @Test
    public void testToString() {
        assertEquals("Transformation en chaîne (flottant)", "01:37:26", sff.toString());
        assertEquals("Transformation en chaîne (HMS)", "01:36:36", sfhms.toString());
    }
}