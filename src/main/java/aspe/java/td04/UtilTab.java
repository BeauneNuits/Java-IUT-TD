package aspe.java.td04;

/**
 * Created on 23/02/15.
 *
 * @author gaugendre
 */
public class UtilTab {
    /**
     * Adds up the elements in the array.
     * @param tab An array of double.
     * @return The sum of the elements in the array.
     */
    public static double sum(double tab[]) {
        double sum = 0;
        for (double n : tab)
            sum += n;
        
        return sum;
    }

    public static double average(double tab[]) {
        if (tab.length == 0)
            return 0;
        return sum(tab)/tab.length;
    }

    public static double min(double tab[]) throws IllegalArgumentException {
        if (tab.length == 0)
            throw new IllegalArgumentException("Empty array.");
        
        double min = tab[0];
        
        for (double n : tab) {
            if (n < min)
                min = n;
        }
        
        return min;
    }

    public static double max(double tab[]) throws IllegalArgumentException {
        if (tab.length == 0)
            throw new IllegalArgumentException("Empty array.");
        
        double max = tab[0];

        for (double n : tab) {
            if (n > max)
                max = n;
        }

        return max;
    }

    public static void print(double tab[]) {
        System.out.print("[");
        int i;
        for (i = 0; i < tab.length - 1; i++) {
            System.out.print(tab[i] + ", ");
        }
        System.out.println(tab[i] + "]");
    }

    /**
     * Copies the values of the first array in the second.
     * The second array must be larger than the first.
     * @param tab The array to copy.
     * @param tab2 The target array where to copy values from the first.
     * @throws IllegalArgumentException When the target array is too small.
     */
    public static void copy(double tab[], double tab2[]) throws IllegalArgumentException {
        if (tab.length > tab2.length) {
            throw new IllegalArgumentException("The target array is too small.");
        }

        System.arraycopy(tab, 0, tab2, 0, tab.length);

        /* This is the old way to do that.
        for (int i = 0; i < tab.length; i++) {
            tab2[i] = tab[i];
        }
        */
    }

    /**
     * Create a new tab with the values of the one given in argument. 
     * @param tab the array to copy.
     * @return The new array with the values of the first.
     */
    public static double[] copy(double tab[]) {
        double[] tab2 = new double[tab.length];
        copy(tab, tab2);
        return tab2;
    }

    /**
     * This method generate an array filled with random values between 0 (included) and 100 (excluded).
     * @param nbItem The number of items in the array.
     * @return An array of double filled with random values.
     */
    public static double[] generateRandArray(int nbItem) {
        double[] tab = new double[nbItem];
        
        for (int i = 0; i < tab.length; i++) {
            tab[i] = Math.random() * 100;
        }
        
        return tab;
    }

    public static float sum(float[][] tab) {
        float sum = 0;
        
        for (float[] t : tab) {
            for (float val : t) {
                sum += val;
            }
        }
        
        return sum;
    }
    
    public static int countItems(float[][] tab) {
        int nbItems = 0;
        
        for (float[] t : tab) {
            nbItems += t.length;
        }
        
        return nbItems;
    }

    public static float average(float[][] tab) {
        int count = countItems(tab);
        if (count == 0)
            return 0;
        
        return sum(tab) / count;
    }

    public static float sumLine(float[][] tab, int indiceL) {
        if (tab.length == 0)
            return 0;
        
        float sum = 0;

        for (float n : tab[indiceL])
            sum += n;

        return sum;
    }

    public static float sumColumn(float[][] tab, int indiceC) {
        float sum = 0;

        for (float[] aTab : tab) {
            if (aTab.length > indiceC)
                sum += aTab[indiceC];
        }

        return sum;
    }

    public static void print(float[][] tab) {
        System.out.print('[');
        int i;
        
        for (i = 0; i < tab.length - 1; i++) {
            print(tab[i]);
            System.out.println(',');
        }
        
        print(tab[i]);
        System.out.println(']');
    }

    public static void print(float tab[]) {
        System.out.print("[");
        int i;
        for (i = 0; i < tab.length - 1; i++) {
            System.out.print(tab[i] + ", ");
        }
        System.out.print(tab[i] + "]");
    }

    public static float[][] copy(float[][] tab) {
        float[][] t = new float[tab.length][];
        
        for (int i = 0; i < tab.length; i++) {
            t[i] = new float[tab[i].length];
            System.arraycopy(tab[i], 0, t[i], 0, t[i].length);
        }
        
        return t;
    }
}
