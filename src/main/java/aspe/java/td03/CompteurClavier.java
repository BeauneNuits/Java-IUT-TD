package aspe.java.td03;

import aspe.java.tools.Keyboard;

/**
 * Created by Adrien on 11/02/2015.
 */
public class CompteurClavier {
    private static int idMax;
    private final int id;

    static {
        int saisie;
        do {
            System.out.println("Saisir numero initial (positif)");
            saisie = Keyboard.readInt();
        } while (saisie < 0);
    }

    public static int getIdMax() {
        return idMax;
    }

    public CompteurClavier() {
        idMax++;
        this.id = idMax;
    }

    public int getId() {
        return this.id;
    }
}
