package aspe.java.td03.geometry.zeroDim;

/**
 * Created on 11/02/15.
 *
 * @author Gabriel Augendre
 * @author Adrien Rabian
 */
public class Point2D implements Cloneable {
    private double x;
    private double y;

    /**
     * Construit un point à partir de ses deux coordonnées cartésiennes.
     * @param x La coordonnée selon l'axe des abscisses.
     * @param y La coordonnée selon l'axe des ordonnées.
     */
    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Déplace le point à partir de sa position actuelle.
     * @param dx La variation selon les abscisses à appliquer.
     * @param dy La variation selon les ordonnées à appliquer.
     */
    public void deplace(double dx, double dy) {
        x += dx;
        y += dy;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ']';
    }

    /**
     * Renvoie la coordonnée du point selon l'axe des abscisses.
     * @return La coordonnée du point selon l'axe des abscisses.
     */
    public double getX() {
        return x;
    }

    /**
     * Renvoie la coordonnée du point selon l'axe des ordonnées.
     * @return La coordonnée du point selon l'axe des ordonnées.
     */
    public double getY() {
        return y;
    }

    @Override
    public Point2D clone() throws CloneNotSupportedException {
        super.clone();
        return new Point2D(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point2D)) return false;

        Point2D point2D = (Point2D) o;

        if (Double.compare(point2D.x, x) != 0) return false;
        return Double.compare(point2D.y, y) == 0;

    }
}
