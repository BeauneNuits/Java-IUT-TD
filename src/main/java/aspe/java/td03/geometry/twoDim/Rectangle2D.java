package aspe.java.td03.geometry.twoDim;

import aspe.java.td03.geometry.oneDim.Segment2D;
import aspe.java.td03.geometry.zeroDim.Point2D;

/**
 * Created on 11/02/15.
 *
 * @author Gabriel Augendre
 * @author Adrien Rabian
 */
public class Rectangle2D implements Surfaceable {
    // Les quatre segments composant le rectangle.
    Segment2D s1;
    Segment2D s2;
    Segment2D s3;
    Segment2D s4;

    /**
     * Construit un rectangle à partir de quatre points.
     * Privé car non protégée. Seuls les constructeurs publics l'appellent en s'assurant
     * de la cohérence des points (qu'ils forment bien un rectangle).
     * @param p1 Le premier point.
     * @param p2 Le premier point.
     * @param p3 Le troisième point.
     * @param p4 Le dernier point.
     */
    private Rectangle2D(Point2D p1, Point2D p2, Point2D p3, Point2D p4) {
        s1 = new Segment2D(p1, p2);
        s2 = new Segment2D(p2, p3);
        s3 = new Segment2D(p3, p4);
        s4 = new Segment2D(p4, p1);
    }

    /**
     * Construit un rectangle sur la base de trois points.
     * Les trois points doivent former un angle droit, peu importe à quel point se situe l'angle.
     * @param p1 Le premier point.
     * @param p2 Le second point.
     * @param p3 Le dernier point.
     * @throws IllegalArgumentException Lorsque les points ne forment pas un angle droit.
     */
    public Rectangle2D(Point2D p1, Point2D p2, Point2D p3) throws IllegalArgumentException {
        Point2D p4;

        // On calcule le dernier point. Pour ça il faut vérifier que les points forment bien un angle droit
        // et découvrir quel est le point sur lequel se situe l'angle.
        if (scalaire(p1, p2, p3) == 0) {
            // L'angle droit est sur p1
            p4 = new Point2D(p3.getX() + p2.getX() - p1.getX(), p3.getY() + p2.getY() - p1.getY());
        } else if (scalaire(p2, p1, p3) == 0) {
            // L'angle droit est sur p2
            p4 = new Point2D(p1.getX() + p3.getX() - p2.getX(), p1.getY() + p3.getY() - p2.getY());
        } else if (scalaire(p3, p2, p1) == 0) {
            // L'angle droit est sur p3
            p4 = new Point2D(p1.getX() + p2.getX() - p3.getX(), p1.getY() + p2.getY() - p3.getY());
        } else {
            // Pas d'angle droit
            throw new IllegalArgumentException("Les trois points ne forment pas un angle droit.");
        }

        // On construit ensuite les segments normalement. On appelle pas le constructeur à quatre points
        // parce qu'on en a pas la possibilité : il faudrait que ce soit la première instruction.
        s1 = new Segment2D(p1, p2);
        s2 = new Segment2D(p2, p4);
        s3 = new Segment2D(p4, p3);
        s4 = new Segment2D(p3, p1);
    }

    /**
     * Construit un rectangle sur la base de deux points formant la diagonale.
     * Le rectangle sera supposé aligné sur les axes des ordonnées et des abscisses.
     * @param p1 Une extrémité de la diagonale.
     * @param p2 L'autre extrémité de la diagonale.
     */
    public Rectangle2D(Point2D p1, Point2D p2) {
        this(p1, new Point2D(p2.getX(), p1.getY()), p2, new Point2D(p1.getX(), p2.getY()));
    }

    @Override
    public double surface() {
        return s1.longueur() * s2.longueur();
    }

    @Override
    public double perimetre() {
        return 2*(s1.longueur() + s2.longueur());
    }

    /**
     * Calcule le produit scalaire des vecteurs p1-p2 et p1-p3.
     * @param p1 Le point commun.
     * @param p2 Un point pour former l'angle.
     * @param p3 Le dernier point.
     * @return Le produit scalaire.
     */
    private static double scalaire(Point2D p1, Point2D p2, Point2D p3) {
        double x1 = p2.getX() - p1.getX();
        double y1 = p2.getY() - p1.getY();
        double x2 = p3.getX() - p1.getX();
        double y2 = p3.getY() - p1.getY();

        return x1 * x2 + y1 * y2;
    }
}
