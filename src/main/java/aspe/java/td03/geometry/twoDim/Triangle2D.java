package aspe.java.td03.geometry.twoDim;

import aspe.java.td03.geometry.oneDim.Segment2D;
import aspe.java.td03.geometry.zeroDim.Point2D;

/**
 * Created on 11/02/15.
 *
 * @author Gabriel Augendre
 * @author Adrien Rabian
 */
public class Triangle2D implements Surfaceable {
    private final Segment2D s1;
    private final Segment2D s2;
    private final Segment2D s3;

    /**
     * Construit un triangle à partir de trois points.
     * @param p1 Le premier point.
     * @param p2 Le second point.
     * @param p3 Le dernier point.
     */
    public Triangle2D(Point2D p1, Point2D p2, Point2D p3) {
        s1 = new Segment2D(p1, p2);
        s2 = new Segment2D(p2, p3);
        s3 = new Segment2D(p3, p1);
    }

    @Override
    public double surface() {
        double l1 = s1.longueur();
        double l2 = s2.longueur();
        double l3 = s3.longueur();
        return Math.sqrt((l1 + l2 + l3)*(-l1 + l2 + l3)*(l1 - l2 + l3)*(l1 + l2 - l3))/4;
    }

    @Override
    public double perimetre() {
        return s1.longueur() + s2.longueur() + s3.longueur();
    }
}
