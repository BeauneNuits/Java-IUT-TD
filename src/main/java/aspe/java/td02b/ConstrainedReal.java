package aspe.java.td02b;

import aspe.java.tools.Keyboard;

/**
 * @author Gabriel Augendre
 * Represents a restricted real number.
 */
public class ConstrainedReal {
    private final double min;
    private final double max;
    private double val;

    public ConstrainedReal(double min, double max) {
        this(min, max, min);
    }

    public ConstrainedReal(double min, double max, double val) {
        if (min > max)
            throw new ArithmeticException("Interval not valid : min > max.");
        if (val < min || val > max)
            throw new ArithmeticException("Number is not between min and max");

        this.min = min;
        this.max = max;
        this.val = val;
    }

    @Override
    public String toString() {
        return val + " in [" + min + ", " + max + "]";
    }

    /**
     * Set the new number. The new number must be between the bounds, otherwise an {@link java.lang.ArithmeticException}
     * will be thrown.
     * @param num The number to set. Must be between the bounds.
     * @throws ArithmeticException if the specified number is not between the bounds.
     */
    public void setVal(double num) throws ArithmeticException{
        if (num < min || num > max)
            throw new ArithmeticException("Number not valid (out of interval [" + this.min + ", " + this.max + "]");
        this.val = num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConstrainedReal)) return false;

        ConstrainedReal that = (ConstrainedReal) o;

        if (Double.compare(that.max, max) != 0) return false;
        if (Double.compare(that.min, min) != 0) return false;
        return Double.compare(that.val, val) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(min);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(max);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(val);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getVal() {
        return val;
    }

    @Deprecated
    public void changeNum() {
        System.out.println("Please enter a number to replace " + this.val);
        double val = Keyboard.readDouble();
        if (val < min || val > max) {
            System.out.printf("Number not valid (out of interval [%f, %f]).\n", this.min, this.max);
            changeNum();
        } else
            this.val = val;
    }
}
