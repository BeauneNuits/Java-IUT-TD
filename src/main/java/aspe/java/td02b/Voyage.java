package aspe.java.td02b;

import info.augendre.java.td02.ConstrainedInteger;

/**
 * Created on 21/01/15.
 *
 * @author gaugendre
 */
public class Voyage {
    private static final int MIN_LENGTH = 1;
    private static final int MAX_LENGTH = 30000;
    private static final double MIN_KM_PRICE = 0.05;
    private static final double MAX_KM_PRICE = 3.2;

    private String cityDep;
    private String cityArr;
    private final ConstrainedInteger length;
    private final ConstrainedReal kmPrice;

    /**
     * Create a {@link Voyage} with a departure, an arrival, a length and a per kilometer price.
     * Two voyages with the same city of departure and the same city of arrival will be easily comparable in terms of
     * price and length.
     * @param cityDep The departure city of the voyage.
     * @param cityArr The arrival city of the voyage.
     * @param length The length of the voyage in kilometers.
     * @param kmPrice The per kilometer price of the voyage.
     */
    public Voyage(String cityDep, String cityArr, int length, double kmPrice) {
        this.cityDep = cityDep;
        this.cityArr = cityArr;
        this.length = new ConstrainedInteger(MIN_LENGTH, MAX_LENGTH, length);
        this.kmPrice = new ConstrainedReal(MIN_KM_PRICE, MAX_KM_PRICE, kmPrice);
    }

    @Override
    public String toString() {
        return "" + cityDep + " --> " + cityArr +
                " (" + length.getNum() + " km, " +
                kmPrice.getVal() * length.getNum() + " €)";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Voyage)) return false;

        Voyage voyage = (Voyage) o;

        if (cityArr != null ? !cityArr.equals(voyage.cityArr) : voyage.cityArr != null) return false;
        if (cityDep != null ? !cityDep.equals(voyage.cityDep) : voyage.cityDep != null) return false;
        if (kmPrice != null ? !kmPrice.equals(voyage.kmPrice) : voyage.kmPrice != null) return false;
        if (length != null ? !length.equals(voyage.length) : voyage.length != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = cityDep != null ? cityDep.hashCode() : 0;
        result = 31 * result + (cityArr != null ? cityArr.hashCode() : 0);
        result = 31 * result + (length != null ? length.hashCode() : 0);
        result = 31 * result + (kmPrice != null ? kmPrice.hashCode() : 0);
        return result;
    }

    /**
     * Compares the length of two {@link Voyage} with the same departure/arrival.
     * @param voyage The voyage to be compared.
     * @return a negative integer, zero, or a positive integer as the length of this object
     *         is less than, equal to, or greater than the length of the specified object.
     * @throws IllegalArgumentException if the voyages don't have the same departure/arrival.
     */
    public int compareLength(Voyage voyage) throws IllegalArgumentException {
        if (!this.compareStartStop(voyage))
            throw new IllegalArgumentException("The specified Voyage doesn't have the same departure/arrival.");

        int lengthThis = length.getNum();
        int lengthOther = voyage.length.getNum();

        return lengthThis < lengthOther ? -1 : lengthThis == lengthOther ? 0 : 1;
    }

    /**
     * Compares the price of two {@link Voyage} with the same departure/arrival.
     * @param voyage The voyage to be compared.
     * @return a negative integer, zero, or a positive integer as the price of this object
     *         is less than, equal to, or greater than the price of the specified object.
     * @throws IllegalArgumentException if the voyages don't have the same departure/arrival.
     */
    public int comparePrice(Voyage voyage) throws IllegalArgumentException {
        if (!this.compareStartStop(voyage))
            throw new IllegalArgumentException("The specified Voyage doesn't have the same departure/arrival.");

        double priceThis = kmPrice.getVal() * length.getNum();
        double priceOther = voyage.kmPrice.getVal() * voyage.length.getNum();

        return priceThis < priceOther ? -1 : priceThis == priceOther ? 0 : 1;
    }

    /**
     * Compares this and the specified {@link Voyage} in order to check the departure/arrival.
     * @param voyage The voyage to compare to.
     * @return True if they both have the same departure and arrival, else False.
     */
    public boolean compareStartStop(Voyage voyage) {
        if (this == voyage) return true;
        if (voyage == null) return false;

        if (cityArr != null ? !cityArr.equals(voyage.cityArr) : voyage.cityArr != null) return false;
        if (cityDep != null ? !cityDep.equals(voyage.cityDep) : voyage.cityDep != null) return false;

        return true;
    }

    public String getCityDep() {
        return cityDep;
    }

    public void setCityDep(String cityDep) {
        this.cityDep = cityDep;
    }

    public String getCityArr() {
        return cityArr;
    }

    public void setCityArr(String cityArr) {
        this.cityArr = cityArr;
    }

    public int getLength() {
        return length.getNum();
    }

    public void setLength(int length) {
        this.length.setNum(length);
    }

    public double getKmPrice() {
        return kmPrice.getVal();
    }

    public void setKmPrice(double kmPrice) {
        this.kmPrice.setVal(kmPrice);
    }
}
