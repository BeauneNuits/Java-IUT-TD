package aspe.java.td02b;

import aspe.java.tools.Keyboard;

import java.util.ArrayList;

/**
 * Created on 21/01/15.
 *
 * @author gaugendre
 */
public class TestVoyage {
    public static void main(String[] args) {
        Voyage v1 = new Voyage("Lyon", "Turin", 303, 1.6);
        Voyage v2 = new Voyage("Lyon", "Turin", 351, 1.1);
        Voyage v6 = new Voyage("Lyon", "Turin", 700, 0.2);
        Voyage v3 = new Voyage("Paris", "Lyon", 465, 1.3);
        Voyage v4 = new Voyage("Paris", "Lyon", 465, 1.3);
        Voyage v5 = new Voyage("Bourges", "Nantes", 190, 2);
        ArrayList<Voyage> voyageList = new ArrayList<>(6);
        voyageList.add(v1);
        voyageList.add(v2);
        voyageList.add(v3);
        voyageList.add(v4);
        voyageList.add(v5);
        voyageList.add(v6);

//        printVoyageList(voyageList);

//        v1.setLength(305);
//        v2.setCityArr("Torino");
//        v3.setKmPrice(0.05);
//        v4.setCityDep("Lutèce");
//        printVoyageList(voyageList);

//        compareVoyageRoute(v1, v3);
//        compareVoyageRoute(v1, v2);
//        compareVoyagePrice(v1, v3);
//        compareVoyagePrice(v4, v3);

        System.out.println("From where ? ");
        String dep = Keyboard.readString();
        System.out.println("To where ? ");
        String arr = Keyboard.readString();
        System.out.println();

        ArrayList<Voyage> matchingList = findVoyage(voyageList, dep, arr);

        printVoyageList(matchingList);

        if (matchingList.size() == 0)
            System.out.println("No matching voyage found.\nPlease review your criteria and try again.");
        else {
            Voyage cheapest = cheapestVoyage(matchingList);
            Voyage shortest = shortestVoyage(matchingList);

            if (!cheapest.equals(shortest)) {
                System.out.println("The cheapest voyage matching your criteria is :");
                System.out.println(cheapest);
                System.out.println("The shortest voyage matching your criteria is :");
                System.out.println(shortest);
            } else {
                System.out.println("The cheapest and shortest voyage matching your criteria is :");
                System.out.println(cheapest);
            }
        }

    }

    /**
     * Find all the voyages matching the departure and arrival given.
     * @param voyageList The voyages list to look into.
     * @param dep The departure city.
     * @param arr The arrival city.
     * @return A list containing all the voyages in the specified list that match the criteria.
     */
    public static ArrayList<Voyage> findVoyage(ArrayList<Voyage> voyageList, String dep, String arr) {
        ArrayList<Voyage> returnList = new ArrayList<>();
        for (Voyage v : voyageList) {
            if (v.getCityDep().toLowerCase().equals(dep.toLowerCase()) &&
                    v.getCityArr().toLowerCase().equals(arr.toLowerCase()))
                returnList.add(v);
        }

        return returnList;
    }

    /**
     * Simply prints a voyage list.
     * @param al The voyage list to print.
     */
    public static void printVoyageList(ArrayList<Voyage> al) {
        System.out.println("==== LIST OF VOYAGES ====");
        if (al.size() > 0) {
            for (int i = 0; i < al.size(); i++) {
                System.out.printf("%d. ", i + 1);
                System.out.println(al.get(i));
            }
        } else {
            System.out.println("Nothing here...");
        }
        System.out.println();
    }

    /**
     * This method compares all the voyages in a list and gives you the cheapest.
     * This method is designed to be used with a list of voyages that all have the same departure/arrival.
     * @param voyageList The voyages to compare. They must all have the same departure/arrival.
     * @return The cheapest of them all !
     * @throws IllegalArgumentException When a voyage in the list doesn't have the same departure/arrival.
     */
    public static Voyage cheapestVoyage(ArrayList<Voyage> voyageList) throws IllegalArgumentException {
        Voyage cheapest = voyageList.get(0);
        for (Voyage v : voyageList) {
            if (v.comparePrice(cheapest) < 0) cheapest = v;
        }

        return cheapest;
    }

    /**
     * This method compares all the voyages in a list and gives you the shortest.
     * This method is designed to be used with a list of voyages that all have the same departure/arrival.
     * @param voyageList The voyages to compare. They must all have the same departure/arrival.
     * @return The shortest of them all !
     * @throws IllegalArgumentException When a voyage in the list doesn't have the same departure/arrival.
     */
    public static Voyage shortestVoyage(ArrayList<Voyage> voyageList) throws IllegalArgumentException {
        Voyage shortest = voyageList.get(0);
        for (Voyage v : voyageList) {
            if (v.compareLength(shortest) < 0) shortest = v;
        }

        return shortest;
    }
}
