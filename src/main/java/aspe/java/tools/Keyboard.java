package aspe.java.tools;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class used to read keyboard's inputs from user.
 */

public class Keyboard {
	/**
	 * Read a String from standard input.
	 * @return The String read from standard input.
	 */
	public static String readString() {
		Scanner in = new Scanner(System.in);
		return in.nextLine();
	}

	/**
	 * Read a float from standard input.
	 * @return The float read from standard input.
	 */
	public static float readFloat() throws InputMismatchException {
		Scanner in = new Scanner(readString());

		while (in.hasNext()) {
			if (in.hasNextFloat())
				return in.nextFloat();
			in.next();
		}

		if (!in.hasNextFloat()) {
			System.err.println("Error in data input : No float.");
			throw new InputMismatchException("No float to read.");
		}

		return 0.f; // Never reached but needed.
	}

	/**
	 * Read a double from standard input.
	 * @return The double read from standard input.
	 */
	public static double readDouble() throws InputMismatchException {
		Scanner in = new Scanner(readString());

		while (in.hasNext()) {
			if (in.hasNextDouble())
				return in.nextDouble();
			in.next();
		}

		if (!in.hasNextDouble()) {
			System.err.println("Error in data input : No double.");
			throw new InputMismatchException("No double to read.");
		}

		return 0.; // Never reached but needed.
	}

	/**
	 * Read an int from standard input.
	 * @return The int read from standard input.
	 */
	public static int readInt() throws InputMismatchException {
		Scanner in = new Scanner(readString());

		while (in.hasNext()) {
			if (in.hasNextInt())
				return in.nextInt();
			in.next();
		}

		if (!in.hasNextInt()) {
			System.err.println("Error in data input : No int.");
			throw new InputMismatchException("No int to read.");
		}

		return 0; // Never reached but needed.
	}

	/**
	 * Read a char from standard input.
	 * @return The char read from standard input.
	 */
	public static char readChar() throws InputMismatchException {
		String line = readString();

		if (line.length() <= 0) {
			System.err.println("Error in data input : No char.");
			throw new InputMismatchException("No char to read.");
		}

		return line.charAt(0);
	}

	/**
	 * Testing method for current class.
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		char c;
		System.out.print("Please enter something with a character within : ");
		c = readChar();
		System.out.printf("You typed : '%c'", c);
	}
}


