package aspe.java.td01;

import aspe.java.tools.Keyboard;

/**
 * This class intends to compute all possible ways
 * to make a given value in euros (integer value)
 * with coins of 1, 0.50, 0.20, 0.10 and 0.05 euros.
 *
 * @author Gabriel Augendre
 * @author Adrien Rabian
 */
public class Exercise5 {
	public static void main(String[] args) {
		boolean visible = true;
		System.out.println("Welcome ! Please enter the total amount in euros : ");
		int total = Keyboard.readInt();
		System.out.println("1e\t50c\t20c\t10c\t5c\tto make " + total);

		total *= 100; // Making the total in cents rather than in euros.
		double combos = 0; // Combination count.

		//stop:
		for (int eu = 0; eu <= (total / 100); eu++) { // Counting 1 euro coins.
			int totalLessEu = total - eu * 100;

			for (int cqte = 0; cqte <= totalLessEu / 50; cqte++) { // Counting 50cts coins.
				int totalLessEuCqte = totalLessEu - cqte * 50;

				for (int vgt = 0; vgt <= totalLessEuCqte / 20; vgt++) { // Counting 20cts coins.
					int totalLessEuCqteVgt = totalLessEuCqte - vgt * 20;

					for (int dix = 0; dix <= totalLessEuCqteVgt / 10; dix++) { // Counting 10cts coins.
						int cq = totalLessEuCqteVgt - dix * 10;
						cq /= 5; // Counting 5cts coins.
						combos++;
						if (combos < 0)
							throw new ArithmeticException("Count overflow");
						// Next line prints the combination.
						if (visible) {
							System.out.println(eu + "\t" + cqte + "\t" + vgt + "\t" + dix + "\t" + cq);
							System.out.println("Do you want to show next combinations" +
									"(will continue to count) ? (o/N) ");
							char cont = Keyboard.readChar();
							if (Character.toLowerCase(cont) != 'o')
								visible = false;
						}
					}
				}
			}
		}

		System.out.println(combos + " combination(s) found.");
	}
}
