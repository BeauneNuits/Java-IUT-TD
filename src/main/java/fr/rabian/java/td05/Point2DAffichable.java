package fr.rabian.java.td05;

/**
 * Created by adrien on 05/03/15.
 */
public class Point2DAffichable extends Point2D implements JsonNotation {

    @Override
    public String toString() {
        return this.classNotation() + " {" + this.objectNotation() + "\n}";
    }

    public String classNotation() {
        return this.getClass().getSimpleName();
    }

    public String objectNotation() {
        return "\n\tx: " + this.getX() + ",\n\ty: " + this.getY();
    }

    public void affiche() {
        System.out.println(this);
    }
}
