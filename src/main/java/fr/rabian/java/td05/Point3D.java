package fr.rabian.java.td05;

/**
 * Created by adrien on 12/03/15.
 */
public class Point3D extends Point2D {
    private int z;

    public void initialise(int abs, int ord, int z) {
        super.initialise(abs, ord);
        this.z = z;
    }

    public int getZ() {
        return z;
    }
}
