package fr.rabian.java.td05;

/**
 * Created by adrien on 04/03/15.
 */
public interface JsonNotation {

    String classNotation();
    String objectNotation();

    @Override
    String toString();
}
