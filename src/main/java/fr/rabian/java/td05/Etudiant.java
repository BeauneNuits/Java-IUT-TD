package fr.rabian.java.td05;

/**
 * Created by Adrien on 01/03/2015.
 */
public class Etudiant extends Personne {
    private final long noEtu;

    public Etudiant (String nom, String prenom, long noEtu) {
        super(nom, prenom);
        this.noEtu = noEtu;
    }

    public Etudiant (Personne p, long noEtu) {
        super(p);
        this.noEtu = noEtu;
    }

    public long getNoEtu() {
        return noEtu;
    }
}
