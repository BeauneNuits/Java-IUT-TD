package fr.rabian.java.td05;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created on 13/03/15.
 *
 * @author arabian
 * @author gaugendre
 */
public abstract class Json implements JsonNotation {
    public String classNotation() {
        return this.getClass().getSimpleName();
    }
    public String objectNotation() {
        Object[] fields = getAllFields(new LinkedList<>(), this.getClass()).toArray();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < fields.length; i++) {
            Field f = (Field) fields[i];
            f.setAccessible(true);
            if (i != 0)
                sb.append(',');
            try {
                sb.append("\n\t").append(f.getName()).append(": ").append(f.get(this));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null)
            fields = getAllFields(fields, type.getSuperclass());

        return fields;
    }

    @Override
    public String toString() {
        return classNotation() + " {" + this.objectNotation() + "\n}";
    }
}
