package fr.rabian.java.td05;

/**
 * Created by adrien on 05/03/15.
 */
public class Point2D {

    private int x, y;

    public void initialise(int abs, int ord) {
        x = abs;
        y = ord;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
