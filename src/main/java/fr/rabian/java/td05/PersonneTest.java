package fr.rabian.java.td05;

/**
 * Created by adrien on 04/03/15.
 */
public class PersonneTest {
    public static void main(String[] args) {
        Personne ar = new Personne("Rabian", "Adrien");
        Etudiant are = new Etudiant(ar, 11407349);
        EtudiantBoursier areb = new EtudiantBoursier(are, are.getNoEtu());

        ar.affiche();
        are.affiche();
        areb.affiche();
    }
}
