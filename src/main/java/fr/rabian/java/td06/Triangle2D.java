package fr.rabian.java.td06;

/**
 * Created on 11/02/15.
 *
 * @author Gabriel Augendre
 * @author Adrien Rabian
 */
public class Triangle2D extends fr.rabian.java.td05.Json implements Surfaceable, Cloneable, FormeGeometrique2D {
    private final Segment2D s1;
    private final Segment2D s2;
    private final Segment2D s3;

    /**
     * Construit un triangle à partir de trois points.
     * @param p1 Le premier point.
     * @param p2 Le second point.
     * @param p3 Le dernier point.
     */
    public Triangle2D(Point2D p1, Point2D p2, Point2D p3) {
        s1 = new Segment2D(p1, p2);
        s2 = new Segment2D(p2, p3);
        s3 = new Segment2D(p3, p1);
    }

    @Override
    public double surface() {
        double l1 = s1.longueur();
        double l2 = s2.longueur();
        double l3 = s3.longueur();
        return Math.sqrt((l1 + l2 + l3)*(-l1 + l2 + l3)*(l1 - l2 + l3)*(l1 + l2 - l3))/4;
    }

    @Override
    public double perimetre() {
        return s1.longueur() + s2.longueur() + s3.longueur();
    }

    @Override
    public Triangle2D clone() {
        return new Triangle2D(s1.getP1(), s1.getP2(), s2.getP2());
    }

    public void deplace(float dx, float dy) {
        Segment2D[] tab = {s1, s2, s3};

        for (Segment2D s : tab) {
            s.deplace(dx, dy);
        }
    }

    public void affiche() {
        System.out.println(this);
    }

    public boolean estIdentique(Object o) {
        return this.equals(o);
    }
}
