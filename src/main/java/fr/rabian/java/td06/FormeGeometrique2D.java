package fr.rabian.java.td06;

/**
 * Created by adrien on 24/03/15.
 */
public interface FormeGeometrique2D extends Affichable
{
    void deplace(float dx, float dy) ;
    boolean estIdentique(Object o) ;
}
