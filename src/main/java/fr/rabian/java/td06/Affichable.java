package fr.rabian.java.td06;

/**
 * Created by adrien on 24/03/15.
 */
public interface Affichable
{
    void affiche() ;
}
