package fr.rabian.java.td06;

/**
 * Created on 11/02/15.
 * Cette classe réalise une agrégation.
 *
 * @author Gabriel Augendre
 * @author Adrien Rabian
 */
public class Segment2D implements Cloneable, FormeGeometrique2D {
    private final Point2D p1;
    private final Point2D p2;

    /**
     * Construit un segment avec ses deux extrémités.
     * @param p1 Une extrémité du segment.
     * @param p2 L'autre extrémité du segment.
     */
    public Segment2D(Point2D p1, Point2D p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    /**
     * Calcule et retourne la longueur du segment.
     * @return La longueur du segment.
     */
    public double longueur() {
        return Math.sqrt(Math.pow(p2.getX() - p1.getX(), 2) + Math.pow(p2.getY() - p1.getY(), 2));
    }

    @Override
    public String toString() {
        return "De " + p1.toString() + " à " + p2.toString() + ".";
    }

    /**
     * Cette méthode affiche la description textuelle du segment dans le flux de sortie par défaut.
     */
    public void affiche() {
        System.out.println(this);
    }

    /**
     * Déplace la première extrémité du segment.
     * @param dxP1 La variation selon les abscisses à appliquer.
     * @param dyP1 La variation selon les ordonnées à appliquer.
     */
    public void deplaceP1(float dxP1, float dyP1) {
        p1.deplace(dxP1, dyP1);
    }

    /**
     * Déplace la deuxième extrémité du segment.
     * @param dxP2 La variation selon les abscisses à appliquer.
     * @param dyP2 La variation selon les ordonnées à appliquer.
     */
    public void deplaceP2(float dxP2, float dyP2) {
        p2.deplace(dxP2, dyP2);
    }

    /**
     * Renvoie un clone de la première extrémité du segment.
     * @return Un clone de la première extrémité du segment.
     */
    public Point2D getP1() {
        try {
            return p1.clone();
        } catch (CloneNotSupportedException cse) {
            cse.printStackTrace(System.err);
        }
        return null;
    }

    /**
     * Renvoie un clone de la seconde extrémité du segment.
     * @return Un clone de la seconde extrémité du segment.
     */
    public Point2D getP2() {
        try {
            return p2.clone();
        } catch (CloneNotSupportedException cse) {
            cse.printStackTrace(System.err);
        }
        return null;
    }

    public void deplace (float dx, float dy) {
        deplaceP1(dx, dy);
        deplaceP2(dx, dy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Segment2D segment2D = (Segment2D) o;

        if (!p1.equals(segment2D.p1)) return false;
        if (!p2.equals(segment2D.p2)) return false;

        return true;
    }

    public boolean estIdentique(Object o) {
        return equals(o);
    }

    public Segment2D clone() {
        return new Segment2D(getP1(), getP2());
    }
}
