package fr.rabian.java.td06;

/**
 * Created on 11/02/15.
 *
 * @author Gabriel Augendre
 * @author Adrien Rabian
 */
public interface Surfaceable {
    /**
     * Calcule et renvoie la surface de la forme géométrique considérée.
     * @return La surface de la forme.
     */
    double surface();

    /**
     * Calcule et renvoie le périmètre de la forme géométrique considérée.
     * @return Le périmètre de la forme géométrique considérée.
     */
    double perimetre();
}
