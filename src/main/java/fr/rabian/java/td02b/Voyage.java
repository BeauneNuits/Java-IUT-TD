package fr.rabian.java.td02b;

/**
 * Created by Adrien on 02/02/2015.
 */
public class Voyage {
    private final String vDep;
    private final String vArr;
    private final ReelContraint length;
    private final ReelContraint prixKm;

    public Voyage() {
        vDep = null;
        vArr = null;
        length = null;
        prixKm = null;
    }

    public Voyage(String vDep, String vArr, double length, double prixKm) throws IllegalArgumentException {
        this.vDep = vDep;
        this.vArr = vArr;
        this.length = new ReelContraint(1, 30000, length);
        this.prixKm = new ReelContraint(0.05, 3.2, prixKm);
    }
}
