package fr.rabian.java.td02b;

/**
 * Cette classe représente un réel contraint.
 * @author Adrien Rabian
 */
public class ReelContraint {
    /**
     * Borne inférieure de l'intervalle
     */
    private final double min;
    /**
     * Borne supérieure de l'intervalle
     */
    private final double max;
    /**
     * Valeur du réel
     */
    private double val;

    /**
     * Constructeur de l'intervalle
     * @param min Borne inf.
     * @param max Bone sup.
     */
    public ReelContraint(double min, double max) {
        this(min, max, min);
    }

    /**
     * Constructeur avec valeur
     * @param min Borne inf.
     * @param max Bone sup.
     * @param val Valeur
     * @throws ArithmeticException Si intervalle et/ou valeur non correcte
     */
    public ReelContraint(double min, double max, double val) throws IllegalArgumentException {
        if (min <= max && val >= min && val <= max) {
            this.min = min;
            this.max = max;
            this.val = val;
        } else {
            if (min > val) {
                throw new IllegalArgumentException("La valeur de la borne inférieure doit être inférieure à la valeur de la borne supérieure.");
            } else {
                throw new IllegalArgumentException("La valeur doit être comprise dans l'intervalle. " + val + " n'est pas acceptable.");
            }
        }
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getVal() {
        return val;
    }

    /**
     * Permet de définir la valeur de le réel
     * @param val Valeur
     * @throws IndexOutOfBoundsException Si hors des bornes
     */
    public void setVal(double val) throws IndexOutOfBoundsException {
        if (val >= this.min && val <= this.max) {
            this.val = val;
        } else {
            throw new IndexOutOfBoundsException("La valeur doit être comprise dans l'intervalle : [" + this.min + " , " + this.max + "]. " + val + " n'est pas acceptable.");
        }
    }

    @Override
    public String toString() {
        return Double.toString(this.val);
    }

    public void affiche() {
        System.out.println(this.toString());
    }

    /**
     * Permet d'incrémenter le réel
     * @param incrValue Incrément
     * @throws IndexOutOfBoundsException Si la valeur calculée dépasse les bornes de l'intervalle
     */
    public void increment (double incrValue) throws IndexOutOfBoundsException {
        double newVal = this.val + incrValue;
        if (newVal >= this.min && newVal <= this.max) {
            this.val = newVal;
        } else {
            throw new IndexOutOfBoundsException("La valeur doit être comprise dans l'intervalle : [" + this.min + " , " + this.max + "]. \n" +
                    newVal + " n'est pas acceptable.");
        }
    }
}
