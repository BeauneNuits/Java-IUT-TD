package fr.rabian.java.td02;

/**
 * Created by Adrien on 25/01/2015.
 */
public interface Sexagesimal {
    int getH();
    int getM();
    int getS();
    double getDec();
    String toString();
}
