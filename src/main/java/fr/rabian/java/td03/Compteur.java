package fr.rabian.java.td03;


/**
 * Created by Adrien on 11/02/2015.
 */
public class Compteur {
    private static int idMax = 0;
    private final int id;

    public static int getIdMax() {
        return idMax;
    }

    public Compteur() {
        idMax++;
        this.id = idMax;
    }

    public int getId() {
        return this.id;
    }
}
