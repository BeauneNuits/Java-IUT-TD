package fr.rabian.java.td03;

public class Point2D {
    private double x;
    private double y;

    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point2D() {
        this(0, 0);
    }

    public Point2D(double abs) {
        this(abs, 0);
    }

    public void deplace(double dx, double dy) {
        x += dx;
        y += dy;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ']';
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance (Point2D pt) {
        return Math.sqrt(Math.pow(this.x + pt.x, 2) + Math.pow(this.y + pt.y, 2));
    }

    public void rotation(double angleEnRadians) {
        double xn = this.x*Math.cos(angleEnRadians) - this.y*Math.sin(angleEnRadians);
        double yn = this.x*Math.sin(angleEnRadians) + this.y*Math.cos(angleEnRadians);
        this.x = xn;
        this.y = yn;
    }
}
