package info.augendre.java.td02;

/**
 * Implementation using a real number to store data.
 * There will be only a float to represent all data.
 */
public class SexagesimalF extends Sexagesimal {
    private final double deci;

    public SexagesimalF(int hours, int minutes, int seconds) {
        this.deci = hours + minutes / 60.0 + seconds / 3600.0;
    }

    public SexagesimalF(double deci) {
        this.deci = deci;
    }

    public double getDec() {
        return deci;
    }

    public int getH() {
        return (int)this.deci;
    }

    public int getM() {
        int hours = (int)this.deci;
        return (int)((this.deci - hours) * 60);
    }

    public int getS() {
        int hours = (int)this.deci;
        return (int)((this.deci - hours) * 3600) % 60;
    }
}
