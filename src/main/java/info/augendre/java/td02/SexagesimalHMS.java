package info.augendre.java.td02;

/**
 * Implementation using integers to store the number.
 * We'll so have an int for hours, another for minutes, and a last for seconds.
 */
public class SexagesimalHMS extends Sexagesimal {
    private final int hours;
    private final int minutes;
    private final int seconds;

    public SexagesimalHMS(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public SexagesimalHMS(double flottant) {
        int hours = (int)flottant;
        this.hours = hours;
        this.seconds = (int)((flottant - hours) * 3600) % 60;
        this.minutes = (int)((flottant - hours) * 60);
    }

    public double getDec() {
        return hours + minutes / 60.0 + seconds / 3600.0;
    }

    public int getH() {
        return hours;
    }

    public int getM() {
        return minutes;
    }

    public int getS() {
        return seconds;
    }
}
