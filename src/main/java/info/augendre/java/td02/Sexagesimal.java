package info.augendre.java.td02;

/**
 * Created on 29/01/15.
 *
 * @author gaugendre
 */
public abstract class Sexagesimal {
    public abstract double getDec();
    public abstract int getH();
    public abstract int getM();
    public abstract int getS();

    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", this.getH(), this.getM(), this.getS());
    }
}
