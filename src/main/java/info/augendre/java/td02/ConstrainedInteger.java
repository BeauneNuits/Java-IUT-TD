package info.augendre.java.td02;

import aspe.java.tools.Keyboard;

/**
 * @author Gabriel Augendre
 * Represents a restricted integer.
 */
public class ConstrainedInteger {
    private final int min;
    private final int max;
    private int num;

    public ConstrainedInteger(int min, int max) {
        this(min, max, min);
    }

    public ConstrainedInteger(int min, int max, int num) throws ArithmeticException {
        if (min > max)
            throw new ArithmeticException("Interval not valid : min > max.");
        if (num < min || num > max)
            throw new ArithmeticException("Number is not between min and max");

        this.min = min;
        this.max = max;
        this.num = num;
    }

    @Override
    public String toString() {
        return num + " in [" + min + ", " + max + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConstrainedInteger)) return false;

        ConstrainedInteger that = (ConstrainedInteger) o;

        if (max != that.max) return false;
        if (min != that.min) return false;
        return num == that.num;

    }

    @Override
    public int hashCode() {
        int result = min;
        result = 31 * result + max;
        result = 31 * result + num;
        return result;
    }

    /**
     * Set the new number. The new number must be between the bounds, otherwise an {@link java.lang.ArithmeticException}
     * will be thrown.
     * @param num The number to set. Must be between the bounds.
     * @throws ArithmeticException if the specified number is not between the bounds.
     */
    public void setNum(int num) throws ArithmeticException {
        if (num < min || num > max)
            throw new ArithmeticException("Number not valid (out of interval [" + this.min + ", " + this.max + "]");
        this.num = num;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getNum() {
        return num;
    }

    @Deprecated
    public void changeNum() {
        System.out.println("Please enter a number to replace " + this.num);
        int num = Keyboard.readInt();
        if (num < min || num > max) {
            System.out.printf("Number not valid (out of interval [%d, %d]).\n", this.min, this.max);
            changeNum();
        } else
            this.num = num;
    }

    /**
     * Increments the current integer with the step specified in the argument.
     * @param inc The number to add to the current number.
     */
    public void increment(int inc) {
        int num = this.num + inc;

        if (num > max)
            num = max;
        else if (num < min)
            num = min;

        this.num = num;
    }
}
