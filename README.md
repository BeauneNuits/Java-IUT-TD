# Java-IUT-Java-TD
TD/TP de Java à l'IUT

## Build status
Teamcity : [![build status - Teamcity](https://tomcat.rabian.fr/TC/app/rest/builds/buildType:%28id:TDJava_Mvn1%29/statusIcon)](https://tomcat.rabian.fr/TC/viewType.html?buildTypeId=TDJava_Mvn1)

Travis CI : [![build status - travis ci](https://travis-ci.org/Crocmagnon/Java-IUT-TD.svg?branch=master)](https://travis-ci.org/Crocmagnon/Java-IUT-TD)

